# Testat Programmentwurf (Testat-002) für den Kurs TFE19-1 - Info III

Eine Nachrichtenverteilungs (MessageDispatcher) Klasse ermöglicht es mehreren Sendern (Sender) die Nachrichten generieren, diese in einen Nachrichtenspeicher abzulegen, und dann mehreren Empfängern (Receiver) das Empfangen der Nachrichten.
Jede Nachricht kann für alle oder für einen speziellen Empfänger sein. Basierend auf dieser Regel die Verteilung realisieren.

Die Festlegung der Anzahl von Sendern und Empfängern kann statisch erfolgen. 
Pluspunkte bei Dynamischer Festlegung (durch übergabe von Parametern auf der Konsole beim Aufruf des Programms).

Die Identifizierung der Empfänger kann einfach ihr Index sein. 
Pluspunkte bei andere Arten die Empfänger zu identifizieren und somit durch die Absender zu addressieren.

Es ist Ihnen überlassen wie Sie die Datenstrukturen realisieren wollen.
Die Sender werden nach neue Nachrichten abgefragt.
Empfänger bekommen die Nachrichten zugestellt.

Eine Nachricht soll zumindest einen Integer beinhalten, sowie eine Identifizierung der Empfänger der Nachricht (ein spezieller, oder alle). 

Der Inhalt der Nachrichten sollte für jeden einzelnen Sender nicht immer gleich sein.

Jeder Empfänger druckt bei Erhalt den Inhalt der an ihn addressierten Nachrichten auf der Konsole.
Wenn eine Nachricht allen addressierten Empfängern zugestellt wurde, wird diese durch den Nachrichtenverteiler zerstört.

Ein extract aus der dazugehörigen main.cpp (für den Fall der Statischen Konfiguration von Anzahl Sender und Empfänger, sowie die Indexbasierte Identifizierung):

```cpp
#define SENDER_AMOUNT 10
#define RECEIVER_AMOUNT 5
#define TEST_RUN_LENGTH 10

// ...

int main(int argc, char **argv)
{
  // ...
  std::vector<Receiver> receivers; 
  // ...
  std::vector<Sender> senders;

  MessageDispatcher messageDispatcher(senders, SENDER_AMOUNT, receivers, RECEIVER_AMOUNT);

  for (int testRun = 0; testRun < TEST_RUN_LENGTH; testRun++)
  {
      for (int i = 0; i < SENDER_AMOUNT; i++)
      {
          messageDispatcher.put(senders[i].generateMessage());
      }

      messageDispatcher.dispatchAllMessages(); // All messages known to messageDispatcher are dispatched to the receivers
  }
  // ...
}
```

Zur Bearbeitung der Lösung erstellen Sie zunächst einen Branch ``solution/testat-002`` in Ihrem Fork. Einigen Sie sich hierbei innerhalb welchen Forks Sie in der Gruppe kollaborieren.

Fügen Sie mich ``danielcesarini`` und Ihre Gruppenmitglieder zu diesem Projekt hinzu.

```bash
# Aktualisieren des Repository
git fetch upstream
# Das wird unsere Basis fuer den Merge Request
git checkout -b testat-002 upstream/testat-002
git push -u origin testat-002

# Der Branch für die Loesung
git checkout -b solution/testat-002 upstream/testat-002

# An die Arbeit
git add ...
git commit ...
git push -u origin solution/testat-002
```

-------------------------

## Aufgabenstellung

Erarbeiten Sie die Klassen Receiver, Sender, MessageDispatcher, erstellen Sie falls nötig neue Methoden um die gestellte Aufgabe zu lösen.

In Ordner testat-002 finden Sie schon hpp und cpp files. Diese dienen als Basis für die Ausarbeitung.

Ihre Receiver Klasse muss mindestens die Methoden implementieren:

```cpp
void receive(MyMessage receivedMesssage);
```

Ihre Sender Klasse muss mindestens die Methoden implementieren:

```cpp
MyMessage* generateMessage();
```

Ihre MessageDispatcher Klasse muss mindestens die Methoden implementieren:

```cpp
void dispatchAllMessages();
void putMessage(MyMessage* messageToPut);
```

Pluspunkte bei der Realisierung von MyMessage als Klasse, anstatt als Struct.

### Allgemeines

Die Aufgabe ist durch die Gruppe am 10.03.2021 in alfaview zu präsentieren, Dauer der Präsentation 5 Minuten. 
Anschließend erfolgt eine Durchsprache des Quellcodes in GitLab mit dem Dozenten. 
Hierbei können die Teilnehmer Fragen gestellt bekommen welche Teil der Bewertung sind, damit die individuelle Leistung erfasst werden kann.

Achten Sie bitte bei der Abgabe auf folgende Punkte:

* [ ] Die Abgabe (über GitLab Merge Request) erfolgt bis spätestens 09.03.2021 - 20.00 Uhr, und durch eine kurze Präsentation (möglichst durch ein paar Folien in PDF - auch im GitLab abzulegen) der Teilnehmer in alfaview am 10.03.2021 ab 16.30.
* [ ] Verwenden Sie git als Quellcodeverwaltung. Die Aufgabe gilt als abgegeben sobald Sie in GitLab einen Merge-request erstellt haben.
* [ ] Der Code, sowie der git Commit Verlauf werden durch den Dozenten betrachtet.
* [ ] Die Methoden sind entsprechend dokumentiert.
* [ ] Kommentare und Variablennamen ausschließlich in Englischer Sprache.
* [ ] Verwenden Sie einen durchgängigen Formatierungsstil.

### Pluspunkte

Zusätzlich können Sie Ihre Note durch die Umsetzung der folgenden Features deutlich verbessern:

* [ ] Implementierung der Operatoren `[]`, `=` und der Copy Constructors in den Klassen wo diese nützlich sein können.
* [ ] Einlesen der Informationen zur Konfiguration des Programms über die Konsole mittels der Bibliothek CLI.
* [ ] Die Realisierung der Nachrichten als selbstständige Klasse.
* [ ] Die Implementierung von einen Speicher für Nachrichten auf seiten der Empfänger für Nachrichten welche spezielle / gewisse Eigenschaften aufweisen werden (erfordert die Realisierung von einer Form von Filter).
* [ ] Die Implementierung von spezifischen Regeln für den Zugriff (https://de.wikipedia.org/wiki/Zugriffskontrolle) auf den Inhalt von Nachrichten - z.B. mittels einer Access Control List (https://de.wikipedia.org/wiki/Access_Control_List) - um auf seite der Sender einer Nachricht gewisse Eigenschaften zu vergeben, welche den Zugriff für Ausgewählte Empfänger erlauben, und für andere nicht.
* [ ] Verwendung von Exceptions.
* [ ] Ausführliche Auswertung und Interpretation der Testergebnisse. Hierfür dürfen auch PDFs eingecheckt werden.
* [ ] Automatisiertes erzeugen einer API Dokumentation mittels Doxygen.
* [ ] Überraschen Sie mich....