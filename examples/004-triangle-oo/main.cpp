#include "triangle.hpp"
int main(void) {
  Triangle t1; /* Default-Konstruktor */
  Triangle t3(1.0,2.0,3.0); /* Konstruktor mit 3 double Parametern */
  Triangle t4(t1); /* Kopier-Konstruktor*/
  Triangle t5(&t3); /* Initialisierung durch Zeiger auf Triangle */
  return 0;
}
