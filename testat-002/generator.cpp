/*********************************************************************************************/
/**
 *@file   generator.cpp
 *@brief  Methodes for the Generator class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include "generator.hpp"

Generator::Generator(std::vector<std::string> &senderNames, std::vector<std::string> &receiverNames)
:m_senderNames(senderNames), m_receiverNames(receiverNames)
{

    object[0] = "Das Haus ";
    object[1] = "Das Auto ";
    object[2] = "Die Person ";

    verb[0] = "steht ";
    verb[1] = "faehrt ";
    verb[2] = "laeuft ";

    subject[0] = "gerade.";
    subject[1] = "schnell.";
    subject[2] = "langsam.";

    srand(time(NULL));

}


std::string Generator::generateString()
{
    
    std::string message;
    message += object[rand() % 3];
    message += verb[rand() % 3];
    message += subject[rand() % 3];
    message += std::to_string(rand() % 11);

    return message;

}

int Generator::gererateMessageType()
{
    return ((rand() % 3) +1);
}

std::vector<std::string> Generator::generateReceiver(int mType)
{
    std::vector <std::string> retVal;
    if (mType == Message::messageType::UNICAST)
    {
        retVal.push_back(m_receiverNames [rand() % m_receiverNames.size()]);

    }
    else if (mType == Message::messageType::MULTICAST)
    {
        int count = rand() % m_receiverNames.size() + 1;
        std::vector<std::string> pickVector;
        pickVector = m_receiverNames;
        for(int i=0; i<count; i++)
        {
            int index = rand() % pickVector.size();
            retVal.push_back(pickVector [index]);
            pickVector.erase(pickVector.begin()+index);
        }

    }
    return retVal;

}

Message* Generator::generateMessage(std::string name)
{
        // vector receiver, content, 
    int msgType = gererateMessageType();
    std::vector<std::string> receivers = generateReceiver(msgType);
    Message* message = new Message(name, receivers,generateString(), msgType );
    return message;

}

Generator::~Generator()
{

}