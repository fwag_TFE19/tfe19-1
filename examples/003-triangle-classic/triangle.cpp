#include "triangle.hpp"
#include <cmath>
static double epsilon = 1e-6;
static void getHypotenuse (Triangle *triag) {
  double a = triag->a;
  double b = triag->b;
  double c = triag->c;
  double hypotenuse = 0.0;
  double x = 0;
  double y = 0;
  if((a >= b) && (a >= c)) {
  hypotenuse = a;
  x = b;
  y = c;
  } else if ((b >= a) && (b >= c)) {
  hypotenuse = b;
  x = a;
  y = c;
} else {
  hypotenuse = c;
  x = b;
  y = a;
  }
  triag->a = x;
  triag->b = y;
  triag->c = hypotenuse;
}
double perimeter (Triangle_t triangle) {
  return triangle.a + triangle.b + triangle.c;
}
bool isRectangular(Triangle *triag) {
  getHypotenuse(triag);
  double a = triag->a;
  double b = triag->b;
  double c = triag->c;
  bool res = false;
  double delta = std::abs((c*c) - ((a*a) + (b*b)));
  if(delta < epsilon ) {
  res = true;
  } else {
  res = false;
  }
  return (res);
}
double area(Triangle &triag) {
  getHypotenuse(&triag);
  return (triag.a*triag.b)/2.0;
}
