/*********************************************************************************************/
/**
 *@file   main.cpp
 *@brief  Main functionality of the testat-002 program
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <CLI11.hpp>

#include "config.h"
#include "sender.hpp"
#include "receiver.hpp"
#include "message.hpp"
#include "messagedispatcher.hpp"
#include <string>
#include <vector>
#include "generator.hpp"
#include "commandlineoutput.hpp"
#include "cliparse.hpp"
#include <exception>
#include "names.hpp"

/**
 * @brief Handles the setup and the testing functionality of the project
 * 
 * @param argc Parameter to be parsed by CLI11
 * @param argv Parameter to be parsed by CLI11
 * @return int Returnvalue to the operating system
 */
int main(int argc, char **argv)
{
  CliParseClass startupProgram(argc,argv);
  int recAmount = startupProgram.getReceiverAmount();
  int sendAmount = startupProgram.getSenderAmount();
  int testSize = startupProgram.getTestSize();
  
  cmdInfo("####    MESSAGE PROGRAM     #####");

  //Printing the current values of Senders, Receivers and tests for debugging issues
  cmdInfo(fmt::format("Value of Senders: {}", sendAmount));
  cmdInfo(fmt::format("Value of Receivers: {}", recAmount));
  cmdInfo(fmt::format("Number of Tests: {}", testSize));

  std::vector<std::string> senderNames;
  for(int i=0;i<sendAmount;i++){
    senderNames.push_back(NAMES_LIST[NAMES_LIST_COUNT+i]);
  }

  std::vector<std::string> receiverNames;
  for(int i=0;i<recAmount;i++){
    receiverNames.push_back(NAMES_LIST[i]);
  }
  
  Generator gen(senderNames,receiverNames);

  
  //Generator Test
  /*
  for(int i = 0;i<200;i++){
  cmdInfo(gen.generateMessage("Gisela")->printState());
  }
  */

  //Sender Test
  /*
  Sender testSender("Test", &gen);
  fmt::print("Adress of test sender: {}!\n", testSender.getAdress());
  testSender.setMessage(testSender.generateMessage());
  fmt::print("Message: {}!\n", testSender.getMessageContent());
  */

  //Acknowledge Test
  /*
  Message testMessage("GieselasTestSender", receiverNames,"TestContent",Message::MULTICAST);
  testMessage.acknowledge("Peter");
  cmdInfo(testMessage.printState());
  */

  std::vector<Sender*> senders;
  for(std::string curName : senderNames){
    senders.push_back(new Sender(curName,&gen));
  }

   std::vector<Receiver*> receivers;
  for(std::string curName : receiverNames){
    receivers.push_back(new Receiver(curName));
  }

  MessageDispatcher msgDispatcher(senders,receivers);

  for(int i=0;i<testSize;i++){
    senders[i]->setMessage(senders[i]->generateMessage());
    cmdInfo(senders[i]->getMessage()->printState());
  }

  msgDispatcher.dispatchAllMessages();

  return 0;
}
