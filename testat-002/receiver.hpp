/*********************************************************************************************/
/**
 *@file   receiver.hpp
 *@brief  Header file for the Receiver class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef RECEIVER_HPP
#define RECEIVER_HPP

#include "message.hpp"

/**
 * @brief Receives the message 
 * 
 */
class Receiver {
public:
    /**
     * @brief Construct a new Receiver object
     * 
     * @param adress Adress (e.g. name) of the receiver
     */
    Receiver(std::string adress);

    /**
     * @brief Destroy the Receiver object
     * 
     */
    ~Receiver();

    /**
     * @brief Receives the message and acknowledges the receive
     * 
     * @param receivedMessage Pointer to the message object that is going to be received
     */
    void receive(Message* receivedMessage);

    /**
     * @brief Get the Adress (e.g. name) of a Receiver
     * 
     * @return std::string Returns the adress (e.g. name) of the receiver 
     */
    std::string getAdress();

    
private:
    /**
     * @brief Adress of the receiver
     * 
     */
    std::string m_adress;

};

#endif // !RECEIVER_HPP