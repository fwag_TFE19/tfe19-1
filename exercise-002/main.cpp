#include <fmt/format.h>
#include <fmt/chrono.h>
#include <CLI11.hpp>

#include "config.h"

/**
 * All none initialized or zero initialized global variables will be put into the
 * .bss section during linkage
 */
int a;
int c = 0;

/**
 * All initialized global variables will be put into the
 * .data section during linkage
 */
int d = 42;

/**
 * All constant initialized global variables will be put into the
 * .rodata section during linkage
 */
const int e = 42;

int static_test()
{
  // all static variables will be treated as global variables
  static int a = 0;
  a = a + 1;
  return a;
}

//extern int foo; --> no longer visible due to the usage of static in data.cpp

extern const int& get_foo_reference();
extern void print_foo();

int * myFunction()
{
  int a = 0;
  return &a;
}

int main(int argc, char **argv)
{

  int b;
  static int f = 0;
  const static int g = 42;

  int * pointer;
  pointer = myFunction();
  
/*
  int foo = 92;
  fmt::print("foo {} \n", foo); // 92
  {
    int foo = 84;
    fmt::print("foo {} \n", foo); // 84
  }
  fmt::print("foo {} \n", foo); // 92
  */

  /* just for the sake of completeness */
  fmt::print("sizeof int {} \n", sizeof(int));

  fmt::print("The address of a: {} value of a {}\n", fmt::ptr(&a),a);
  fmt::print("The address of c: {} value of c {}\n", fmt::ptr(&c),c);
  fmt::print("The address of d: {} value of d {}\n", fmt::ptr(&d),d);
  fmt::print("The address of e: {} value of e {}\n", fmt::ptr(&e),e);

  fmt::print("The address of b: {} value of b {}\n", fmt::ptr(&b),b);
  fmt::print("The address of f: {} value of f {}\n", fmt::ptr(&f),f);
  fmt::print("The address of g: {} value of g {}\n", fmt::ptr(&g),g);


  fmt::print("value of static_test {}\n", static_test());
  fmt::print("value of static_test {}\n", static_test());
  fmt::print("value of static_test {}\n", static_test());
  fmt::print("value of static_test {}\n", static_test());
  fmt::print("value of static_test {}\n", static_test());

  const int &foo = get_foo_reference();
  fmt::print("value of foo {}\n", foo);
  //foo = foo + 1;
  print_foo();

  return 0; /* exit gracefully*/
}
