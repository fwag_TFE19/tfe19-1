/*********************************************************************************************/
/**
 *@file   generator.hpp
 *@brief  Header file for the Generator class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef GENERATOR_HPP
#define GENERATOR_HPP


#include <string>
#include "message.hpp"
#include <vector>
#include <random>
/**
 * @brief Generates a random message. This message consists of the message type, the message content and the receiver adress list.
 * 
 */
class Generator
{
    public:

    /**
     * @brief Construct a new Generator object
     * 
     * @param senderNames Initialize the generator object with a std::vector of strings witch stores all posible senders
     * @param receiverNames Initialize the generator object with a std::vector of strings witch stores all posible receivers
     */
    Generator(std::vector <std::string> &senderNames, std::vector <std::string> &receiverNames);

    /**
     * @brief Destroy the Generator object
     * 
     */
    ~Generator();

    /**
     * @brief Generates the message content
     * 
     * @return std::string Random message content
     */
    std::string generateString();

    /**
     * @brief Generates the type of message
     * 
     * @return int Pre defined message type (UNICAST, MULTICAST, BROADCAST, NOMESSAGE)
     */
    int gererateMessageType();

    /**
     * @brief Generates the list of recivers based on the message type
     * 
     * @param mType (UNICAST, MULTICAST, BROADCAST, NOMESSAGE)
     * @return std::vector <std::string> Adresslist of receivers
     */
    std::vector <std::string> generateReceiver(int mType);

    /**
     * @brief Generates the whole message object including the adresslist of receivers, the content and the message type
     * 
     * @param name Name of sender
     * @return Message* Pointer to a new message object
     */
    Message* generateMessage(std::string name);

    private:

    /**
     * @brief Array of the available objects (used to generate the message content)
     * 
     */
    std::string object[3];

    /**
     * @brief Array of the available subjects (used to generate the message content)
     * 
     */
    std::string subject[3];

    /**
     * @brief Array of the available verbs (used to generate the message content)
     * 
     */
    std::string verb[3];

    /**
     * @brief Stores the sender names
     * 
     */
    std::vector <std::string> m_senderNames;

    /**
     * @brief Stores the receiver names
     * 
     */
    std::vector <std::string> m_receiverNames;

};

#endif //GENERATOR_HPP