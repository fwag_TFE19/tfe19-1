/*********************************************************************************************/
/**
 *@file   cliparse.cpp
 *@brief  Methodes for the CliParseClass implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include "cliparse.hpp"


CliParseClass::CliParseClass(int argc, char **argv)
:m_argc(argc),m_argv(argv){

    this->initProject();
}

CliParseClass::~CliParseClass(){

}

void CliParseClass::initProject(){

    //Defaultvalues if no parameters were added when running the executable
    this->m_senderAmount = 5; 
    this->m_receiverAmount = 8;
    this->m_testSize = 0;
    
    CLI::App app{ PROJECT_NAME };
    try {
        app.set_version_flag("-V,--version", fmt::format("{} {}",PROJECT_VER,PROJECT_BUILD_DATE));

        //Adding the parameters "-s" (amount of senders), "-r" (amount of receivers) and "-t" (number of tests) to CLI11
        app.add_option("-s", m_senderAmount, "Amount of Senders");
        app.add_option("-r", m_receiverAmount, "Amount of Receiver");
        app.add_option("-t", m_testSize, "Number of tests");


        app.parse(this->m_argc, this->m_argv);

        //Validate the passed variables; throws a std::exception if variable is out of range
        if (m_receiverAmount <= 0)
        {
           throw std::range_error("There must be at least 1 receiver!");
        }
        if (m_senderAmount <= 0)
        {
            throw std::range_error("There must be at least 1 sender!");
        }

        if (m_receiverAmount > NAMES_LIST_COUNT)
        {
            throw std::range_error("Reached max amount of receivers!");
        }
        if (m_senderAmount > NAMES_LIST_COUNT)
        {
            throw std::range_error("Reached max amount of senders!");
        }
        
        if (m_testSize <= 0)
        {
            //throw std::range_error("The number of tests can not be negative!");
            this->m_testSize = 1;
            cmdWarn("Testsize must be greater than 0! Testsize is now set to 1");
        }

        if (m_testSize>m_senderAmount)
        {
            this->m_testSize = this->m_senderAmount;
            cmdWarn("Testsize cannot be greater than sendersize, testsize is now equal to sendersize!");
        }


        } catch (const CLI::ParseError &e) {
        //return app.exit(e);
    }
}

int CliParseClass::getSenderAmount(){
    return this->m_senderAmount;
}

int CliParseClass::getReceiverAmount(){
    return this->m_receiverAmount;
}

int CliParseClass::getTestSize(){
    return this->m_testSize;
}