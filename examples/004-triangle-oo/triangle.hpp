/*
 * Include guard verhindert die Mehrfachdefinition
 */
#ifndef TRIANGLE_HPP_
#define TRIANGLE_HPP_
/**
 * Die Klasse Triangle
 */
class Triangle
{
public: /* Sichtbarbeit: oeffentlich */
  /* Member Variablen */
  double m_a;
  double m_b;
  double m_c;
  /* Konstruktoren */
  Triangle(void); /* Default Konstruktor */
  Triangle(double a, double b, double c);
  Triangle(Triangle *pTr);
  Triangle(const Triangle &refTr); /* Kopierkonstruktor */
  /* Destruktor */
  ~Triangle(void);
  /* Methoden */
  double perimeter();
  bool isRectangular();
  double area();
};
#endif /* TRIANGLE_HPP_ */
