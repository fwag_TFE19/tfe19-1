# Übungsaufgabe Nr. 2

Diese Übungsaufgabe wurde im Unterricht gemeinsam erarbeitet. Lerninhalt sind die unterschiedlichen Speicherbereiche eines C++ Programms

**HINWEIS:** Bitte kompilieren Sie diese Aufgabe in der Konfiguration ``Release``, da manche Compiler in der ``Debug`` Konfiguration Variablen auf dem Stack mit ``0`` oder einem anderen festen Wert vorinitialisieren.

# Fragen im Laufe der Vorlesung

## Reihenfolge von static und const

Im Verlauf der Übung kam die Frage auf ob es ``static const`` oder ``const static`` heißen soll. Es gibt keine vorgeschriebene Reihenfolge wie etwa bei Operatoren. Die wohl am häufigsten vertretene Schreibweise ist ``static const``. Das Schlüsselwort ``static`` wird als ``storage class specifier`` bezeichnet.

# Lerninhalte

- Die unterschiedlichen Segmente ``.bss``, ``.data`` und ``.rodata``
- Der Unterschied zwischen Variablen innerhalb von Funktionen und globalen Variablen

# Vertiefung

Welche Ausgabe liefern die folgenden Quellcodefragmente

```cpp
void static_test2()
{
  static int a; 
  float b;
  fmt::print("The address of a: {} value of a {}\n", fmt::ptr(&a),a);
  fmt::print("The address of b: {} value of b {}\n", fmt::ptr(&b),b);
}
```

```cpp
auto a = 0; 
float b;

int main(int argc, char **argv)
{
  fmt::print("The address of a: {} value of a {}\n", fmt::ptr(&a),a);
  fmt::print("The address of b: {} value of b {}\n", fmt::ptr(&b),b);

  return 0; /* exit gracefully*/
}
```
```cpp
int main(int argc, char **argv)
{
  auto a = 0; 
  float b;

  fmt::print("The address of a: {} value of a {}\n", fmt::ptr(&a),a);
  fmt::print("The address of b: {} value of b {}\n", fmt::ptr(&b),b);

  return 0; /* exit gracefully*/
}
```

```cpp
int main(int argc, char **argv)
{
  static const auto a = 0; 
  float b;

  fmt::print("The address of a: {} value of a {}\n", fmt::ptr(&a),a);
  fmt::print("The address of b: {} value of b {}\n", fmt::ptr(&b),b);

  return 0; /* exit gracefully*/
}
```