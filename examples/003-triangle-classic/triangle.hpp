/* Include guard */
#ifndef TRIANGLE_HPP__
#define TRIANGLE_HPP__
typedef struct Triangle {
  double a;
  double b;
  double c;
  double perimeter;
} Triangle_t;
double perimeter (Triangle_t triangle);
bool isRectangular(Triangle *triag);
double area(Triangle &triag);
#endif /*TRIANGLE_HPP__*/