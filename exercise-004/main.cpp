#include <fmt/format.h>
#include <fmt/chrono.h>
#include <CLI11.hpp>
#include <string>

#include "config.h"

struct Ups {
  int m_blubber;
};

class SayHello {
public:

  class Ups {
  public:
    int m_value = 42;
  };

  SayHello(const std::string& name);
  ~SayHello() {
    fmt::print("This is the DTOR of SayHello\n");
  }

  void print() {
    fmt::print("Hello {}\n",m_name);
  }

   /*
    * setter methode for variable m_hello
    */
  bool setHello(const std::string &name)
  {
    auto ret = false;
    if(name.size() > 5)
    {
      ret = false;
    }
    else
    {
      m_hello = name;
      ret = true;
    }
    return ret;
  }

  const std::string& getHello()
  {
    return m_hello;
  }

private:
  std::string m_hello;
  std::string m_name;
  SayHello() = delete;
  SayHello::Ups m_ups;
  //Ups m_outer;

};

SayHello::SayHello(const std::string& name)
:m_name(name),m_hello("Ups")
{
  fmt::print("Value of Ups {}\n",m_ups.m_value);
  //fmt::print("Value of outer Ups {}\n",m_outer.m_blubber); // namespace conflict!!!!
}

//using namespace std; // Please
/*
void cout()
{

}
*/
int main(int argc, char **argv)
{

  /**
   * CLI11 is a command line parser to add command line options
   * More info at https://github.com/CLIUtils/CLI11#usage
   */
  CLI::App app{ PROJECT_NAME };
  try {
    app.set_version_flag("-V,--version", fmt::format("{} {}",PROJECT_VER,PROJECT_BUILD_DATE));
    app.parse(argc, argv);
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  //SayHello hello(); // we have defined a function returning a SayHello object here
  //SayHello hello;
  SayHello say_hello("Paula");
  //hello.m_hello = "Ups"; // not possible due to the fact the variable is private.
//  hello.setHello("Hugo");
//  hello.print();
  say_hello.print();
/*
  const std::string& helloRef = hello.getHello();
  fmt::print("Value of helloRef {}\n",helloRef);
  hello.setHello("Franz");
  fmt::print("Value of helloRef {}\n",helloRef);
  //helloRef = "Test";
*/


  return 0; /* exit gracefully*/
}
