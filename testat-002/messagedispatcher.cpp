/*********************************************************************************************/
/**
 *@file   messagedispatcher.cpp
 *@brief  Methodes for the MessageDispatcher class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include "messagedispatcher.hpp"

MessageDispatcher::MessageDispatcher(std::vector<Sender*> senders,std::vector<Receiver*> receivers)
:m_senders(senders),m_receivers(receivers)
{

}

int MessageDispatcher::available(){
    return m_messageBuffer.size();
}

void MessageDispatcher::dispatchAllMessages(){
    //Create a string List of the Receivers
    std::vector<std::string> receiverStringList;
    for(Receiver* rec : m_receivers){
        receiverStringList.push_back(rec->getAdress());
    }

    //Add all Messages from the Senders to Buffer
    for(Sender* curSend : m_senders){
        //Pop the Message from the Sender, if NULL there is no Message
        Message* msg = curSend->popMessage();   
        if (msg != NULL){
            if(msg->getMessageType() == Message::BROADCAST){
                msg->setReceivers(receiverStringList);
            } 
            this->putMessage(msg);
        }
    }

    //cmdInfo(this->printState());

    //Local pointer to currently processing Message
    Message* curMsg;
    //While there are still Messages to be processed
    while(this->available()>0){
        //Choose the Last Message in the Buffer
        curMsg = m_messageBuffer.back();
        //Loop over all intended Receivers of the Message
        for(std::string curRecStr : curMsg->getIntendendReceivers()){
            //find the Receiver Object from the Reciever Adress/Name and call receive() on the Receiver, passing the Messagepointer to Receiver
            //Receivers will call acknowlage wich will shorten recieverList in Message
            findReceiver(curRecStr)->receive(curMsg);
        }
        //If the current Message doesnt have any Receivers left
        if(curMsg->getIntendendReceivers().size() == 0){
            delete curMsg;
            //delete the Pointer to the Message
            m_messageBuffer.pop_back();
        }
    }
}

Receiver* MessageDispatcher::findReceiver(std::string receiverName){
    std::vector<Receiver*> recList;
    for(Receiver* curRec : m_receivers){
        if(curRec->getAdress() == receiverName){
            recList.push_back(curRec);
        }
    }

    if(recList.size() == 0){
        //Message for a nonexistant Receiver
    }else if(recList.size() > 1){
        //Multiple Receivers with same Name is illegal
    }else{
        //One Receiver found
    }

    return recList.front();

}

void MessageDispatcher::putMessage(Message* messageToPut){
    m_messageBuffer.insert(m_messageBuffer.begin(),messageToPut);
}

MessageDispatcher::~MessageDispatcher(){
    for(Message* curMsg : m_messageBuffer){
        delete curMsg;
    }
}

std::string MessageDispatcher::printState(){
    std::string sds = "[";
    for(Sender* s : m_senders){
        sds = sds + s->getAdress() +",";
    }
    sds += "],";

    std::string recs = "[";
    for(Receiver* r : m_receivers){
        recs = recs + r->getAdress() + ",";
    }
    recs += "],";

    std::string msgs = "[\n";
    for(Message* m : m_messageBuffer){
        msgs = msgs + "\t" + m->printState() + ",\n";
    }
    msgs += "]\n";

    return "Message Dispatcher:(senders,receivers,messages)[\n" + sds + "\n" + recs + "\n" + msgs + "]";

}