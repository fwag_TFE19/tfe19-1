/*********************************************************************************************/
/**
 *@file   sender.cpp
 *@brief  Methodes for the Sender class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include "sender.hpp"
#include "generator.hpp"


Sender::Sender(std::string senderAdress,Generator* rand)
:m_rand(rand), m_senderadress(senderAdress), m_msg(NULL)
{
    
}


Message* Sender::generateMessage()
{
    return m_rand->generateMessage(m_senderadress);
    
}

void Sender::setMessage(Message* msg)
{
    this->m_msg = msg;

}

Message* Sender::getMessage()
{
    return this->m_msg;

}

Message* Sender::popMessage(){
    Message* msg;
    msg = m_msg;
    m_msg = NULL;
    return msg;
}

std::string Sender::getAdress()
{
    return this-> m_senderadress;
}

std::string Sender::getMessageContent()
{
    return m_msg->getContent();
}

Sender::~Sender()
{

}

