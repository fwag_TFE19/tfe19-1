#include <fmt/format.h>

// -> .data segment
static int foo = 42;

const int& get_foo_reference()
{
    return foo;
}

void print_foo()
{
    fmt::print("Value of foo {} address of foo: {}\n",foo,fmt::ptr(&foo));
}