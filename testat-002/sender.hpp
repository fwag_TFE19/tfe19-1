/*********************************************************************************************/
/**
 *@file   sender.hpp
 *@brief  Header file for the Sender class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef SENDER_HPP
#define SENDER_HPP

#include "message.hpp"
#include <string>
#include <vector>
#include "generator.hpp"

/**
 * @brief Handles the message, a referemce to the generator and the adresses of the sender
 * 
 */
class Sender {
public:
    /**
     * @brief Construct a new Sender object
     * 
     * @param senderAdress Adress of the sender
     * @param rand Reference to a generator object
     */
    Sender(std::string senderAdress,Generator* rand);

    /**
     * @brief Destroy the Sender object
     * 
     */
    ~Sender();

    /**
     * @brief Generates a random message using the generate message method defined in class generator
     * 
     * @return Message* Pointer to a new generated random message
     */
    Message* generateMessage();

    /**
     * @brief Pass a message to the sender
     * 
     * @param msg Pointer to the message
     */
    void setMessage(Message* msg);

    /**
     * @brief Returns message object from sender. The returned message will than internally be deleted afterwards. Used to dispatch a Message 
     * 
     * @return Message* Returns Pointer to the message about to be dispatched
     */
    Message* popMessage();

    /**
     * @brief Get a Message from the sender
     * 
     * @return Message* Returns the message
     */
    Message* getMessage();

    /**
     * @brief Get the sender adress
     * 
     * @return std::string Returns the sender adress 
     */
    std::string getAdress();

    /**
     * @brief Returns the message's content of the senders message
     * 
     * @return std::string Returns the content of the message
     */
    std::string getMessageContent();

private:
    /**
     * @brief Pointer to message object
     * 
     */
    Message* m_msg;

    /**
     * @brief Pointer to the generator
     * 
     */
    Generator* m_rand;

    /**
     * @brief Adress of the sender (name)
     * 
     */
    std::string m_senderadress;

};

#endif // !SENDER_HPP