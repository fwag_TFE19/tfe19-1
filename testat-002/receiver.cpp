/*********************************************************************************************/
/**
 *@file   receiver.cpp
 *@brief  Methodes for the Receiver class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include "receiver.hpp"
#include "commandlineoutput.hpp"
#include <fmt/format.h>
#include <fmt/chrono.h>


Receiver::Receiver(std::string adress)
:m_adress(adress)
{

}

Receiver::~Receiver(){

}

void Receiver::receive(Message* receivedMessage){
    receivedMessage->acknowledge(m_adress);
    std::string lookup[4]={"NOMESSAGE","UNICAST","MULTICAST","BROADCAST"};
    cmdInfo(fmt::format("\"{}\" received Message(Type:{}):\"{}\"",m_adress,lookup[receivedMessage->getMessageType()],receivedMessage->getContent()));
}

std::string Receiver::getAdress(){
    return m_adress;
}