#include "triangle.hpp"
#include <cstdio>
#include <cmath>
#include <iostream>

int main(void) {
  double perim = 0.0f;
  Triangle myTriangle;
  myTriangle.a = 311.127;
  myTriangle.b = 311.127;
  myTriangle.c = sqrt(pow(myTriangle.a, 2) + pow(myTriangle.b, 2));
  perim = perimeter(myTriangle);
  std::cout << "Perimeter: " << perim << std::endl;
  std::cout << "Triangle is ";
  if (true == isRectangular(&myTriangle))
  {
  std::cout << "rectangular" << std::endl;
  }
  else
  {
  std::cout << "not rectangular" << std::endl;
  }
  std::cout << "Triangle area is: " << area(myTriangle) << std::endl;
  return 0;
}
