/*********************************************************************************************/
/**
 *@file   messagedispatcher.hpp
 *@brief  Header file for the MessageDispatcher class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef MESSAGEDISPATCHER_HPP
#define MESSAGEDISPATCHER_HPP

#include "message.hpp"
#include "sender.hpp"
#include "receiver.hpp"
#include "commandlineoutput.hpp"
#include <string>
#include <vector>

/**
 * @brief Takes care of the message handling functionality
 * 
 */
class MessageDispatcher {
public:

    /**
     * @brief Construct a new Message Dispatcher object
     * 
     * @param senders List of all senders
     * @param receivers List of all receivers
     */
    MessageDispatcher(std::vector<Sender*> senders,std::vector<Receiver*> receivers);

    /**
     * @brief Destroy the Message Dispatcher object and delete all messages from the buffer
     * 
     */
    ~MessageDispatcher();

    /**
     * @brief Dispatch all messages from the message buffer to intendent receivers
     * 
     */
    void dispatchAllMessages();

    /**
     * @brief Add a message at the beginning of the message buffer
     * 
     * @param messageToPut Message that needs to be put
     */
    void putMessage(Message* messageToPut);

    /**
     * @brief Returns the amount of messages to be dispatched from buffer
     * 
     * @return int Size of the message Buffer
     */
    int available();

    /**
     * @brief Returns the pointer ro a receiver object, which addres is passed by the parameter
     * 
     * @param receiverName Name of the receiver that needs to be located
     * @return Receiver* Pointer to specific receiver object
     */
    Receiver* findReceiver(std::string receiverName);

    /**
     * @brief Outputs the status of the message dispatcher
     * 
     * @return std::string Returns a string containing senders, receivers an messages within the objects 
     */
    std::string printState();

    private:
    /**
     * @brief Vector that stores all messages which need to be put
     * 
     */
    std::vector<Message*> m_messageBuffer;

    /**
     * @brief Vector that stores all senders
     * 
     */
    std::vector<Sender*> m_senders;

    /**
     * @brief Vector that stores all receivers
     * 
     */
    std::vector<Receiver*> m_receivers;

};

#endif // !MESSAGEDISPATCHER_HPP