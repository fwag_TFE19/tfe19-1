/*********************************************************************************************/
/**
 *@file   message.hpp
 *@brief  Header file for the Message class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <string>
#include <vector>
#include <fmt/format.h>
#include <fmt/chrono.h>


/**
 * @brief Handles all necessary information of a message including the content, the sender adress, the receiver adresses and the message type
 * 
 */
class Message {
public:
    /**
     * @brief Construct a new Message object
     * 
     * @param senderAdress Adress of the sender
     * @param recieverAdresses Adresses of the receivers
     * @param content content of the message
     * @param messageType Type of the message (UNICAST, MULTICAST, BROADCAST, NOMESSAGE)
     */
    Message(std::string senderAdress,std::vector<std::string> recieverAdresses,std::string content,int messageType);

    /**
     * @brief Destroy the Message object
     * 
     */
    ~Message();

    /**
     * @brief Get the list of intendent receivers
     * 
     * @return std::vector<std::string> Returns a vector of strings including all receiver adresses
     */
    std::vector<std::string> getIntendendReceivers();

    /**
     * @brief Get the Content of this specific Message Object
     * 
     * @return std::string Return the content of the message
     */
    std::string getContent();

    /**
     * @brief Get the current ammount of receivers within the receiver list
     * 
     * @return int Return receiver count
     */
    int getRecieverCount();

    /**
     * @brief Get the lenght of the message content
     * 
     * @return int Return content length
     */
    int getContentLength();

    /**
     * @brief Search for receiver and delete from receiver adress list
     * 
     * @param adress Adress of the receiver that successfully received the message
     */
    void acknowledge(std::string adress);

    /**
     * @brief Checks if all selected receivers have received the message
     * 
     * @return int Returns "1" if read counter equals reciever counter
     */
    int allRecieved();

    /**
     * @brief  Gathers every information of the message object (message content, sender adress, receiver adresses an message type)
     * 
     * @return std::string Return the message state
     */
    std::string printState();

    /**
     * @brief Get the Message Type
     * 
     * @return int Returns the message type
     */
    int getMessageType();

    /**
     * @brief Set the receiver list
     * 
     * @param recs std::vector of strings including all receiver adresses
     */
    void setReceivers(std::vector<std::string> recs);

    /**
     * @brief type of the message
     * 
     */
    enum messageType{
        NOMESSAGE,
        UNICAST,
        MULTICAST,
        BROADCAST
    };

    

private:
    /**
     * @brief Stores the content of a message transmitted
     * 
     */
    std::string m_content;

    /**
     * @brief Stores the senders adress
     * 
     */
    std::string m_senderAdress;

    /**
     * @brief Stores the receiver adresses
     * 
     */
    std::vector<std::string> m_receiverAdresses;

    /**
     * @brief Stores the message type (enum messageType)
     * 
     */
    int m_messageType;
};

#endif // !MESSAGE_HPP