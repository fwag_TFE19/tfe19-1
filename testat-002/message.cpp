/*********************************************************************************************/
/**
 *@file   message.cpp
 *@brief  Methodes for the Message class implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#include "message.hpp"


Message::Message(std::string senderAdress,std::vector<std::string> receiverAdresses,std::string content,int messageType){
    this->m_senderAdress = senderAdress;
    this->m_messageType = messageType;
    this->m_content = content;
    this->m_receiverAdresses = receiverAdresses;
}

Message::~Message(){

}

std::string Message::getContent(){
    std::string str = this->m_content;    
    return str;
}


std::vector<std::string> Message::getIntendendReceivers(){
    std::vector<std::string> vec = this->m_receiverAdresses;
    return vec;
}

int Message::getRecieverCount(){
    return (m_receiverAdresses.size());
}

int Message::getContentLength(){
    int stringSizeBytes = m_content.size();
    int stringLen = stringSizeBytes/8;
    return stringLen;
}

void Message::acknowledge(std::string adress){
    std::vector<std::string>::iterator position = std::find(m_receiverAdresses.begin(), m_receiverAdresses.end(),adress);
    m_receiverAdresses.erase(position);
}

int Message::allRecieved(){
    int retVal = 0;
    if(m_receiverAdresses.size() == 0)
    {
        retVal = 1;
    }
    return retVal; 
}

std::string Message::printState(){

    std::string recs = "[";
    for(std::string currec : m_receiverAdresses){
        recs += currec;
        recs += ",";
    }

    recs += "]";

    return fmt::format("Message:(content,sender,receivers,type):[{},{},{},{}]",m_content,m_senderAdress,recs,m_messageType);
    
}

int Message::getMessageType(){
    return m_messageType;
}

void Message::setReceivers(std::vector<std::string> recs){
    m_receiverAdresses = recs;
}