/*********************************************************************************************/
/**
 *@file   names.hpp
 *@brief  Header file for the names.cpp
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef NAMES_HPP
#define NAMES_HPP

/**
 * @brief Stores a list of names
 * 
 */

extern const int NAMES_LIST_COUNT;

extern const char* NAMES_LIST[];



#endif // NAMES_HPP