/*********************************************************************************************/
/**
 *@file   cliparse.hpp
 *@brief  Header file for the CliParseClass implementation
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/

#ifndef CLIPARSE_HPP
#define CLIPARSE_HPP

#include <CLI11.hpp>
#include <fmt/format.h>
#include <fmt/chrono.h>
#include "config.h"
#include "names.hpp"
#include "commandlineoutput.hpp"

/**
 * @brief Uses the CLI11 funcionality to read in commandline arguments and passes them over to the main funcion
 * 
 */
class CliParseClass
{

    public:
    /**
     * @brief Construct a new Cli Parse Class object
     * 
     * @param argc The argc value from main.cpp
     * @param argv The argv value from main.cpp
     */
    CliParseClass(int argc, char **argv);

    /**
     * @brief Destroy the Cli Parse Class object
     * 
     */
    ~CliParseClass();

    /**
     * @brief Runs several steps to initialise the project includin the add_option command provided by CLI11.
     *          Also validates the passed arguments or sets default values for senderAmount, receiverAmount and testSize
     * 
     */
    void initProject();

    /**
     * @brief Get the amount of senders passed with the flag "-s"
     * 
     * @return int Number of senders; Defautvalue if nothing is passed: 5
     */
    int getSenderAmount();

    /**
     * @brief Get the amount of receivers passed with the flag "-r"
     * 
     * @return int Number of receivers; Defautvalue if nothing is passed: 8
     */
    int getReceiverAmount();

    /**
     * @brief Get the number of tests passed with the flag "-t"
     * 
     * @return int Number of tests; Defautvalue if nothing is passed: 1
     */
    int getTestSize();
    
    private:
    /**
     * @brief Private member to store the argc from main.cpp
     * 
     */
    int m_argc;

    /**
     * @brief Private member to store the argv from main.cpp
     * 
     */
    char **m_argv;

    /**
     * @brief The amount of senders; Run .exe with Flag "-s" to modify; Default value: 5
     * 
     */
    int m_senderAmount;

    /**
     * @brief The amount of receivers; Run .exe with Flag "-r" to modify; Default value: 8
     * 
     */
    int m_receiverAmount;

    /**
     * @brief The amount of receivers; Run .exe with Flag "-t" to modify; Default value:
     * 
     */
    int m_testSize;
};

#endif // !CLIPARSE_HPP