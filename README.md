# Informatik 3

Begleit Project für Informatik 3 bei TFE19-1 an der DHBW Ravensburg Campus Friedrichshafen.

# Übungsaufgaben

- [Übungsaufgabe Nr. 1](exercise-001/README.md)

# Hilfreiche Links

- [Markdown Cheat Sheet](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
- [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
