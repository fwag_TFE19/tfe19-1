#include <fmt/format.h>
#include <fmt/chrono.h>
#include <CLI11.hpp>

#include "config.h"

struct Test {
  Test() :data(65) {
    fmt::print("CTOR\n");
  }
  ~Test() {
    fmt::print("DTOR\n");
  }
  int data;
};

/** INVALID
int& get_reference()
{
  int a = 68;
  return a;
}
**/

void object_test()
{
  Test a;
  fmt::print(">>>>>>>>>>>>>>\n");
  Test* ptr = new Test; // memory leak if no delete follows the new operator
}

Test* get_test_ptr()
{
  Test* ptr = new Test;
  return ptr;
}


int main(int argc, char **argv)
{
  /**
   * CLI11 is a command line parser to add command line options
   * More info at https://github.com/CLIUtils/CLI11#usage
   */
  CLI::App app{ PROJECT_NAME };
  try {
    app.set_version_flag("-V,--version", fmt::format("{} {}",PROJECT_VER,PROJECT_BUILD_DATE));
    app.parse(argc, argv);
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  /**
   * The {fmt} lib is a cross platform library for printing and formatting text
   * it is much more convenient than std::cout and printf
   * More info at https://fmt.dev/latest/api.html 
   */
  fmt::print("Hello, {}!\n", app.get_name());

  /* INVALID due to dangling reference
  int& foo = get_reference();
  fmt::print("value of foo {} address of foo {}\n",foo,fmt::ptr(&foo));
  */

  fmt::print("------------------\n");
  object_test();
  fmt::print("------------------\n");

  Test* ptr = get_test_ptr();
  delete ptr;
  int foo = 100;
  int* pData = new int[foo];
  fmt::print("address of pData {} value of pData[0] = {}\n", fmt::ptr(pData),pData[0]);
  pData[0] = 42;
  fmt::print("address of pData {} value of pData[0] = {}\n", fmt::ptr(pData),pData[0]);
  delete [] pData;
  pData = nullptr;
  //fmt::print("address of pData {} value of pData[0] = {}\n", fmt::ptr(pData),pData[0]); // usage after delete
  pData = new int[foo]; // Memory leak
  fmt::print("address of pData {} value of pData[0] = {}\n", fmt::ptr(pData),pData[0]);
  pData = new int[foo];
  fmt::print("address of pData {} value of pData[0] = {}\n", fmt::ptr(pData),pData[0]);
  delete [] pData;
  int bar = 42;
  int& ref = foo;
  fmt::print("value of ref {} \n", ref);
  fmt::print("value of foo {} \n", foo);
  foo = 99;
  fmt::print("value of ref {} \n", ref);
  fmt::print("value of foo {} \n", foo);
  ref = 69;
  fmt::print("value of ref {} \n", ref);
  fmt::print("value of foo {} \n", foo);
  ref = bar;
  fmt::print("value of ref {} \n", ref);
  fmt::print("value of foo {} \n", foo);

  fmt::print("value of bar {} \n", bar);
  int* ptrBar = &bar;
  *ptrBar = 25;
  fmt::print("value of bar {} \n", bar);

  ptr = get_test_ptr();
  fmt::print("value of Test->data {} \n", ptr->data);

  Test zzz;
  Test& refTest = zzz;
  fmt::print("value of zzz.data {} \n", zzz.data);
  fmt::print("value of refTest.data {} \n", refTest.data);

  fmt::print("address of ref {} \n", fmt::ptr(&ref));
  fmt::print("address of foo {} \n", fmt::ptr(&foo));

  return 0; /* exit gracefully*/
}
