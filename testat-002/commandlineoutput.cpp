/*********************************************************************************************/
/**
 *@file   commandlineoutput.cpp
 *@brief  Functions to output messages to the commandline using fmt
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/
#include "commandlineoutput.hpp"


bool MESSAGE_INFO = true;
bool MESSAGE_WARN = true;
bool MESSAGE_ERROR = true;


void cmdInfo(std::string msg){
    if (MESSAGE_INFO) fmt::print(MESSAGE_PREFIX_INFO + msg + MESSAGE_POSTFIX_INFO + MESSAGE_NEWLINE);
}

void cmdWarn(std::string msg){
    if (MESSAGE_WARN) fmt::print(MESSAGE_PREFIX_WARN + msg + MESSAGE_POSTFIX_WARN + MESSAGE_NEWLINE);
}

void cmdError(std::string msg){
    if (MESSAGE_ERROR) fmt::print(MESSAGE_PREFIX_ERROR + msg + MESSAGE_POSTFIX_ERROR + MESSAGE_NEWLINE);
}
