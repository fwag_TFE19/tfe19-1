#include "triangle.hpp" /* Inkludiert den Header der Klasse */
#include <cmath> /* wird fuer abs benoetigt */
#include <iostream> /* wird fuer cout benoetigt */
/* Epsilon fuer den Vergleich von zwei Float/Double Zahlen*/
static const double epsilon = 1e-6;
/*
 * Default-Konstruktor
 *
 * Mit Initialisierungsliste
 * http://de.wikipedia.org/wiki/Initialisierungsliste
 */
Triangle::Triangle(void):m_a(0.0),m_b(0.0),m_c(0.0) /* Initialisierung mittels Initialisierungsliste */
{
  std::cout << std::endl << "--- Default-Konstruktor ---" << std::endl;
  std::cout << "a: " << m_a << " b: " << m_b << " c: " << m_c << std::endl;
}
/*
 * Konstruktor mit 3 Parametern zur Initialisierung
 */
Triangle::Triangle(double a, double b, double c)
{
  m_a = a;
  m_b = b;
  m_c = c;
  std::cout << std::endl << "--- Konstruktor mit 3 double Parametern ---" << std::
endl;
  std::cout << "a: " << m_a << " b: " << m_b << " c: " << m_c << std::endl;
}
/*
 * Initialisierung mittels Zeiger auf ein Triangle Objekt
 */
Triangle::Triangle(Triangle *pTr)
{
  m_a = pTr->m_a;
  m_b = pTr->m_b;
  m_c = pTr->m_c;
  std::cout << std::endl << "--- Default mit Initialisierung über Zeiger auf ein Triangle Objekt ---" << std::endl;
  std::cout << "a: " << m_a << " b: " << m_b << " c: " << m_c << std::endl;
}
/*
 * Initialisierung mittels einer Referenz auf ein Triangle Objekt
 */
Triangle::Triangle(const Triangle &refTr)
{
  m_a = refTr.m_a;
  m_b = refTr.m_b;
  m_c = refTr.m_c;
  std::cout << std::endl << "--- Default mit Initialisierung über Referenz auf ein Triangle Objekt ---" << std::endl;
  std::cout << "a: " << m_a << " b: " << m_b << " c: " << m_c << std::endl;
}
/*
 * Destruktor des Objektes
 */
Triangle::~Triangle(void)
{
  std::cout << std::endl << ">>> So Long, and Thanks For All The Fish <<" << std::endl;
}
/* Berechnet die Flaeche des Dreiecks */
double Triangle::area()
{
  double s = perimeter()/2.0;
  return std::sqrt( s * (s-m_a) * (s - m_b) * ( s-m_c ) );
}
/* Berechnet den Umfang des Dreiecks */
double Triangle::perimeter()
{
  return m_a + m_b + m_c;
}
/* Ermittels ob ein Dreieck rechtwincklig ist */
bool Triangle::isRectangular()
{
  double a = m_a;
  double b = m_b;
  double c = m_c;
  double hypotenuse = 0.0;
  double x = 0;
  double y = 0;
  bool res = false;
  if((a >= b) && (a >= c)) {
  hypotenuse = a;
  x = b;
  y = c;
  } else if ((b >= a) && (b >= c)) {
  hypotenuse = b;
  x = a;
  y = c;
  } else {
  hypotenuse = c;
  x = b;
  y = a;
  }
  a = x;
  b = y;
  c = hypotenuse;
  double delta = std::abs((c*c) - ((a*a) + (b*b)));
  if(delta < epsilon ) {
  res = true;
  } else {
  res = false;
  }
  return (res);
}