/*********************************************************************************************/
/**
 *@file   commandlineoutput.hpp
 *@brief  Header file for the commandlineoutput.cpp
 *@author Lisa Mayer, Frederic Emmerth, Wagner Florian
 *@date   2021/03
 *
 *
 *********************************************************************************************/
#ifndef COMMANDLINEOUTPUT_HPP
#define COMMANDLINEOUTPUT_HPP

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <string>

static std::string MESSAGE_PREFIX_INFO = "INFO: ";
static std::string MESSAGE_POSTFIX_INFO = "";

static std::string MESSAGE_PREFIX_WARN = "WARN: ";
static std::string MESSAGE_POSTFIX_WARN = "";

static std::string MESSAGE_PREFIX_ERROR = "ERROR: ";
static std::string MESSAGE_POSTFIX_ERROR = "";

static std::string MESSAGE_NEWLINE = "\n";

/**
 * @brief Message type: Info
 * 
 */
extern bool MESSAGE_INFO;

/**
 * @brief Message type: Warning
 * 
 */
extern bool MESSAGE_WARN;

/**
 * @brief Message type: error
 * 
 */
extern bool MESSAGE_ERROR;

/**
 * @brief Outputs a info string to the commandline using fmt
 * 
 * @param msg Info to be outputted to the commandline
 */
void cmdInfo(std::string msg);

/**
 * @brief Outputs a warning string to the commandline using fmt
 * 
 * @param msg Warning to be outputted to the commandline
 */
void cmdWarn(std::string msg);

/**
 * @brief Outputs a error string to the commandline using fmt
 * 
 * @param msg Error to be outputted to the commandline
 */
void cmdError(std::string msg);



#endif // !MESSAGE_COMMANDLINE_OUTPUT